#!/bin/bash

echo "Instalando docker"

curl -fsSL https://get.docker.com/ -o get-docker.sh
sudo sh get-docker.sh

echo "Instalando Kubectl"

curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

echo "Update + jq + net-tools"

apt install jq -y
apt install net-tools -y
apt update -y